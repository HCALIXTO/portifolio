const crypto = require('crypto')
const knex = require('knex')(require('./knexfile'))
module.exports = {
  createUser ({ email, password, name, access }) {
    console.log(`Add user ${email}`)
    const { salt, hash } = saltHashPassword({ password })
    return knex('user').insert({
      email,
      name,
      access,
      salt,
      encrypted_password: hash
    })
  },
  createProject ({ title, description, content, cardtype, cardorder }) {
    console.log(`Add project ${title}`)
    return knex('project').insert({
      title,
      description,
      content,
      cardtype,
      cardorder
    })
  },
  createTag ({ tagname }) {
    console.log(`Add tag ${tagname}`)
    return knex('tag').insert({
      tagname
    })
  },
  createProjectTag ({ projId, tagId }) {
    console.log(`Add project id ${projId} & tag id ${tagId}`)
    return knex('projtag').insert({
      projId,
      tagId
    })
  },
  createVisual ({ projId, visDesc, element, type }) {
    console.log(`Add image with description "${visDesc}"`)
    return knex('visual').insert({
      projId,
      visDesc,
      element,
      type
    })
  },
  authenticate ({ email, password }) {
    console.log(`Authenticating user ${email}`)
    return knex('user').where({ email })
      .then(([user]) => {
        if (!user) return { success: false }
        const { hash } = saltHashPassword({
          password,
          salt: user.salt
        })
        return {success:hash === user.encrypted_password, uID:user.id, uName:user.name  }
      })
  },
  getTags () {
    console.log("Getting tags...");
    return knex('tag')
      .then(([tags]) => {
        console.log(tags);
        return {success: true, tags}
       });
  }
}
function saltHashPassword({password,salt = randomString()}){
  const hash = crypto
    .createHmac('sha512', salt)
    .update(password)
  return {
    salt,
    hash: hash.digest('hex')
  }
}
function randomString () {
  return crypto.randomBytes(4).toString('hex')
}

// Henrique Stuff
