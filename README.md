# Portifolio 

A simple portifolio to showcase my work as a devoloper.

## Documentation

For more information about using Node.js on Heroku, see these Dev Center articles:

- [10 Habits of a Happy Node Hacker](https://blog.heroku.com/archives/2014/3/11/node-habits)
- [Getting Started with Node.js on Heroku](https://devcenter.heroku.com/articles/getting-started-with-nodejs)
- [Heroku Node.js Support](https://devcenter.heroku.com/articles/nodejs-support)
- [Node.js on Heroku](https://devcenter.heroku.com/categories/nodejs)
- [Using WebSockets on Heroku with Node.js](https://devcenter.heroku.com/articles/node-websockets)


## GIT Stuff

To push to our gitlab:
```
git push git@gitlab.com:HCALIXTO/portifolio.git
```
To push/deploy to heroku
```
heroku login
git push heroku master

it should be at: https://calixto.herokuapp.com
```
Side note:
```
e - personal
s - classic for heroku
```
In case of problems with dependencies try:
```
npm uninstall
npm install
```
to clear old shit before update the dependencies
https://docs.npmjs.com/cli/uninstall

Important: Never change npm version on package.json - it will break heroku server (don't know why).

## Runnig local
```
heroku local
```
it will say which port it is in, usually ( http://localhost:5000/ ) 

## View Heroku LOGs
```
heroku logs (show the last 100 lines)
heroku logs -t (tail the logs in real time) - ctrl C to stop it
``` 

## Tutorials

https://scotch.io/tutorials/how-to-deploy-a-node-js-app-to-heroku
https://scotch.io/tutorials/use-ejs-to-template-your-node-application

## Database

https://hackernoon.com/setting-up-node-js-with-a-database-part-1-3f2461bdd77f

```
Add info on migration for knex + heroku
```

## PostgreSQL Installation (Linux)

https://gist.github.com/alistairewj/8aaea0261fe4015333ddf8bed5fe91f8

```
Use for installation: sudo apt-get install postgresql-10
```
to Query from command line:
```
heroku pg:psql
obs: to select user table must call it "user"
\dt (list the tables)
\d (table name) (describe table in detail)
```
to look for why you can't use port 5432
```
netstat -an | grep 5432
```

to stop postgresql:
```
sudo service postgresql stop
```