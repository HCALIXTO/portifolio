const { saltHashPassword } = require('../store')
exports.up = function (knex, Promise) {

return createUserTable()
  .then(encryptPassword)
  .then(createProjectTable)
  .then(createTagTable)
  .then(createProjectTagTable)
  .then(createVisualTable)

  function createUserTable() {
  return knex.schema.createTable('user', function (t) {
    t.increments('id').primary()
    t.string('email').notNullable().unique()
    t.string('password').notNullable()
    t.string('name').notNullable()
    t.integer('access').notNullable()
    t.timestamps(false, true)
  });
  }

  function encryptPassword() {
     return knex.schema
       .table('user', t => {
         t.string('salt').notNullable();
         t.string('encrypted_password').notNullable()
       })
       .then(() => knex('user'))
       .then(users => Promise.all(users.map(convertPassword)))
       .then(() => {
         return knex.schema.table('user', t => {
           t.dropColumn('password')
         })
       })
     function convertPassword (user) {
       const { salt, hash } = saltHashPassword(user.password)
       return knex('user')
         .where({ id: user.id })
         .update({
           salt,
           encrypted_password: hash
         })
     }
  }

  function createProjectTable() {
     return knex.schema.createTable('project', function (t) {
       t.increments('id').primary()
       t.string('title').notNullable().unique()
       t.text('description').notNullable()
       t.text('content').notNullable()
       t.integer('cardtype').notNullable()
       t.integer('cardorder').notNullable()
       t.timestamps(false, true)
     });
 }

  function createTagTable() {
     return knex.schema.createTable('tag', function (t) {
       t.increments('id').primary()
       t.string('tagname').notNullable().unique()
       t.timestamps(false, true)
     });
  }

  function createProjectTagTable() {
     return knex.schema.createTable('projtag', function (t) {
       t.integer('projId').notNullable()
       t.integer('tagId').notNullable()
       t.primary(['projId', 'tagId'])
       t.foreign('projId')
            .references('id')
            .on('project')
       t.foreign('tagId')
            .references('id')
            .on('tag')
       t.timestamps(false, true)
     });
  }

  function createVisualTable() {
     return knex.schema.createTable('visual', function (t) {
       t.increments('visId').notNullable().primary()
       t.integer('projId').notNullable()
       t.string('visDesc').notNullable()
       t.string('element').notNullable()
       t.string('type').notNullable()
       t.foreign('projId')
            .references('id')
            .on('project')
       t.timestamps(false, true)
     });
  }

};

exports.down = function (knex, Promise) {

  return knex.schema.dropTableIfExists('visual')
  .then.dropTableIfExists('projtag')
  .then.dropTableIfExists('tag')
  .then.dropTableIfExists('project')
  .then.dropPasswordTable()
  .then.dropTableIfExists('user')

  function dropPasswordTable() {
     return knex.schema.table('user', t => {
       t.dropColumn('salt')
       t.dropColumn('encrypted_password')
       t.string('password').notNullable()
     })
  }

}