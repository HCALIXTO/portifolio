function runBabyRun(){
const CreateUser = document.querySelector('.CreateUser')
CreateUser.addEventListener('submit', (e) => {
  e.preventDefault()
  const email = CreateUser.querySelector('.email').value
  const password = CreateUser.querySelector('.password').value
  const name = CreateUser.querySelector('.name').value
  const access = CreateUser.querySelector('.access').value
  post('/createUser', { email, password, name, access })
})

const Login = document.querySelector('.Login')
if(Login) {
  Login.addEventListener('submit', (e) => {
    e.preventDefault()
    const email = Login.querySelector('.email').value
    const password = Login.querySelector('.password').value
    post('/login', { email, password })
      .then(({ status }) => {
        if (status === 200) {
          alert('login success')
        }
        else alert('login failed')
      })
  })
}

const CreateProject = document.querySelector('.CreateProject')
CreateProject.addEventListener('submit', (e) => {
  e.preventDefault()
  const title = CreateProject.querySelector('.title').value
  const description = CreateProject.querySelector('.description').value
  const content = CreateProject.querySelector('.content').value
  const cardtype = CreateProject.querySelector('.cardtype').value
  const cardorder = CreateProject.querySelector('.cardorder').value
  post('/createProject', { title, description, content, cardtype, cardorder })
})

const CreateTag = document.querySelector('.CreateTag')
CreateTag.addEventListener('submit', (e) => {
  e.preventDefault()
  const tagname = CreateTag.querySelector('.tagname').value
  console.log({ tagname });
  post('/createTag', { tagname })
})

const CreateProjectTag = document.querySelector('.CreateProjectTag')
CreateProjectTag.addEventListener('submit', (e) => {
  e.preventDefault()
  const projId = CreateProjectTag.querySelector('.projId').value
  const tagId = CreateProjectTag.querySelector('.tagId').value
  post('/createProjectTag', { projId, tagId })
})

const CreateVisual = document.querySelector('.CreateVisual')
CreateVisual.addEventListener('submit', (e) => {
  e.preventDefault()
  const projId = CreateVisual.querySelector('.imgprojId').value
  const visDesc = CreateVisual.querySelector('.visDesc').value
  const element = CreateVisual.querySelector('.element').value
  const type = CreateVisual.querySelector('.type').value
  post('/createVisual', { projId, visDesc, element, type })
})
}
function post (path, data) {
  return window.fetch(path, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  })
}
