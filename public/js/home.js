// Globals
var scrollDestine = 0;
var scrollSource = 0;
// Filter
function filterProjects(el){
  var classes = "";
  $(el).find(':selected').map(function(){
    //This is OR (returns with one or more of the selected tags)
    classes = (classes=="")? ".tag"+this.text : classes+", .tag"+this.text;
    //This is AND (returns projects with all the selected tags)
    //classes = classes+".tag"+this.text;
  }).get();
  if(classes == ""){
    $(".card").fadeIn(300);
  }else{
    console.log(classes);
    $(".card").fadeOut(300);
    //$(classes).fadeIn(300);
    // To avoid C++ bug
    $(classes).each(function(){
      if($(this).hasClass("card")){
        $(this).fadeIn(300);
      }
    });
  }
}

// Interface functions
function clickLogOut(){
   window.location.replace("/logout");
}

function clickLogIn(){
   var logInBtn = document.querySelector("#logInBtn");
   var status = logInBtn.getAttribute('data-status');
   if(status == "closed"){// Open login form
      $("#loginForm").slideDown( 1000);
      $("#loginCancel").slideDown( 1000);
      logInBtn.setAttribute('data-status','open');
      document.querySelector("#logInError").innerHTML = ""
   }else{
      // Submiting the login form
      $.ajax({
        method:"POST",
        url:"/login",
        data:{"email":document.querySelector("#logInEmail").value,
              "password":document.querySelector("#logInPassword").value},
        success : function(data){window.location.reload(false)}
      })
      .fail(failLogIn)
      .done(function(msg){console.log(msg)});
   }
}

function clickCancelLogIn(){
  var logInBtn = document.querySelector("#logInBtn");
  $("#loginForm").slideUp( 1000);
  $("#loginCancel").slideUp( 1000);
  logInBtn.setAttribute('data-status','closed');
}
// Support functions

// Deal with failed login attempt
function failLogIn(data){
  console.log(data.responseText);
  document.querySelector("#logInError").innerHTML = data.responseText;
}

function addExtraTagInput(){
  var extraTag = $("#extraTagForm").clone().attr("id", "");
  $("#newTags").append(extraTag);
  extraTag.slideDown(200);
}

function addExtraImage(el){
  var visuals = $(el).closest(".visualsForm");
  var extraImg = visuals.find("#extraImgForm").clone().attr("id","");
  visuals.find(".img-forms").append(extraImg);
  extraImg.slideDown(200);
  extraImg.change(function(){getSignedRequest(this)});
}

function addExtraVideo(el){
  console.log("clicked")
  var visuals = $(el).closest(".visualsForm");
  var extraVid = visuals.find("#extraVidForm").clone().attr("id","");
  visuals.find(".vid-forms").append(extraVid);
  extraVid.slideDown(200);
}

function clearExtraTagInput(){
  $("#newTags").find(".tagName").val("");
  $("#newTags").find(".extra").remove();
}

// End Support functions

// Gallery functions

function previousGallery(clicked){
  var gallery = $(clicked).closest(".gallery");
  var current = parseInt(gallery.attr("data-current"));
  var total = parseInt(gallery.attr("data-total"));
  var tgt = (current - 1);
  tgt = (tgt > 0)? tgt : (total+tgt)%total;
  galleryChangeImage(gallery, current, tgt)
}

function nextGallery(clicked){
  var gallery = $(clicked).closest(".gallery");
  var current = parseInt(gallery.attr("data-current"));
  var total = parseInt(gallery.attr("data-total"));
  var tgt = (current + 1) % total;
  galleryChangeImage(gallery, current, tgt)
}

function galleryChangeImage(gallery, current, tgt){
  var elements = gallery.find(".element");

  elements.each(function(){
    $(this).fadeOut(100);

    if($(this).attr("data-index") == tgt){
      $(this).fadeIn(200);
    }
  });
  gallery.attr("data-current", tgt);
}
// End Gallery functions
// Database Functions
function genTag(el){
  //{ tagname: "PHP" }
  return {tagname: $(el).find(".tagName").val()};
}
function submitNewTagForm(){
  var tags = $("#newTags").find(".individualTagForm");
  var arr = new Array();
  tags.each(function(){
    if($(this).find(".tagName").val() != ""){
      arr.push(genTag($(this)));
    }
  });
  console.log(arr);
  arr.forEach(function(tag) { console.log(tag) });
  $.ajax({
    method:"POST",
    url:"/createTags",
    data:{"tagS":arr},
    success : function(data){window.location.reload(false)}
  })
  .fail(failNewTags)
  .done(function(msg){console.log(msg)});//window.location.reload(false)
}

function failNewTags(data){
  console.log(data.responseText);
  //document.querySelector("#logInError").innerHTML = data.responseText;
}

function submitProjectForm(el){
  var form = $(el).closest(".overlay");
  var id = form.attr("data-projID");
  if(validateProjectForm(form)){
    var send = {}
    var obj = {};
    obj.id = form.attr("data-projID");
    obj.title = form.find("#projectTitle").val();
    obj.description = form.find("#projectDescription").val();
    obj.content = form.find("#projectContent").val();
    obj.cardtype = form.find("#projectCardType").val();
    obj.cardorder = form.find("#projectOrder").val();
    
    send.project = obj;
    send.projtag = form.find(".search").find(':selected').map(function(){
      return {"tagId":this.value, "projId":id}
    }).get();
    console.log(send);
    $.ajax({
      method:"POST",
      url:"/createProject",
      data:send,
      success : function(data){window.location.reload(false)}
    })
    .fail(failNewTags)
    .done(function(msg){console.log(msg)});
  }
}

function deleteProject(el){
  var form = $(el).closest(".overlay");
  var id = form.attr("data-projID");
  if(validateProjectForm(form)){
    var send = {}
    var obj = {};
    obj.id = form.attr("data-projID");
    obj.title = form.find("#projectTitle").val();
    obj.description = form.find("#projectDescription").val();
    obj.content = form.find("#projectContent").val();
    obj.cardtype = form.find("#projectCardType").val();
    obj.cardorder = form.find("#projectOrder").val();
    
    send.project = obj;
    $.ajax({
      method:"POST",
      url:"/deleteProject",
      data:send,
      success : function(data){window.location.reload(false)}
    })
    .fail(failNewTags)
    .done(function(msg){console.log(msg)});
  }
}

function validateProjectForm(form){
  return true;
}

// Image Upload

/*
  Function to get the temporary signed request from the app.
  If request successful, continue to upload the file using this signed
  request.
*/
function getSignedRequest(el){
  const projId = $(el).attr("data-projId");
  const files = el.files;
  const file = files[0];
  if(file != null){
    const xhr = new XMLHttpRequest();
    xhr.open('GET', `/get-s3-req?projId=${projId}&file-name=${file.name}&file-type=${file.type}`);
    xhr.onreadystatechange = () => {
      if(xhr.readyState === 4){
        if(xhr.status === 200){
          const response = JSON.parse(xhr.responseText);
          uploadFile(file, response.signedRequest, response.url);
        }
        else{
          alert('Could not get signed URL.');
        }
      }
    };
    xhr.send();
  }
}


// Function to carry out the actual PUT request to S3 using the signed request from the server.
function uploadFile(file, signedRequest, url){
  const xhr = new XMLHttpRequest();
  xhr.open('PUT', signedRequest);
  xhr.onreadystatechange = () => {
    if(xhr.readyState === 4){
      if(xhr.status === 200){
        //document.getElementById('preview').src = url;
        //document.getElementById('avatar-url').value = url;
      }
      else{
        alert('Could not upload file.');
      }
    }
  };
  xhr.send(file);
}

function deleteVisual(el){
  const id = $(el).attr("data-visId");
  if(id !== ""){
    var send = {};
    send.id = id;
    $.ajax({
      method:"POST",
      url:"/deleteVisual",
      data:send,
      success : function(data){$(el).closest(".imgWrapper").fadeOut(300).remove()}
    })
    .fail(failNewTags)
    .done(function(msg){console.log(msg)});
  }
}

function deleteTag(el){
  const id = $(el).attr("data-tagId");
  if(id !== ""){
    var send = {};
    send.id = id;
    //console.log(send);
    $.ajax({
      method:"POST",
      url:"/deleteTag",
      data:send,
      success : function(data){$(el).closest(".imgWrapper").fadeOut(300).remove()}
    })
    .fail(failNewTags)
    .done(function(msg){console.log(msg)});
  }
}

function submitVisualsForm(el){
  console.log("submiting videos")
  var newVids = $(el).closest(".visualsForm").find(".vid-forms .newVidWrapper");
  //console.log(newVids);
  if(newVids.length == 0){
    $(el).closest(".visualsForm").fadeOut(300);
    $(el).closest(".visualsForm").find(".img-forms").empty();
    window.location.reload(false);
  }
  newVids.each(function(){
    var video = $(this);
    var send = {};
    send.element = $(this).find(".vidElement").val();
    send.desc = $(this).find(".vidDescription").val();
    send.projId = $(this).attr("data-projId");
    if(send.element != "" && send.desc != ""){
      $.ajax({
        method:"POST",
        url:"/addVideo",
        data:send,
        success : function(data){video.fadeOut(300).remove()}
      })
      .fail(function(msg){video.find(".error").html(msg)})
      .done(function(msg){
        if($(el).closest(".visualsForm").find(".vid-forms .newVidWrapper").length == 0){
          $(el).closest(".visualsForm").fadeOut(300);
          $(el).closest(".visualsForm").find(".img-forms").empty();
        }
      });
    }else{
      console.log("You mus fill all the boxes for the video")
    }
  })

}

function em(num){
  return  parseFloat(($('html').css('font-size')))*num;
}

// Loading functions
$(document).ready(function(){
  run();
});
window.onload = function () {
  
}

$(window).resize(function(){
  if($(window).innerWidth() > em(41)) {
    $("header").css("display", "grid");
    $(".headerBackGround").css("display", "none");
  }/*else{
    $("header").css("display", "none");
    $(".headerBackGround").css("display", "none");
  }*/
});

function run(){
  $(".search").each(function(){
    var id = $(this).attr("id");
    new SlimSelect({
      select: "#"+id,
      allowDeselect: true
    })
  });

  /* CSS set up */
  $(".overlay").height($("body").height()+10);
  /* To make the popup/card work */
  $(".overlay").click(function(){
    $(this).fadeOut(400);
    if($(this).attr("data-reposition") != "no"){
      document.documentElement.scrollTop = scrollSource;//document.body.scrollTop
    }
  });
  $(".popup").click(function(e){e.stopPropagation()});
  $(".card").click(function(){
    //console.log($(this).attr("data-tgt"));
    $($(this).attr("data-tgt")).fadeIn(400);
    scrollSource = document.documentElement.scrollTop;//document.body.scrollTop
    document.documentElement.scrollTop = 10;//document.body.scrollTop
    new SlimSelect({select: '.search',allowDeselect: true})
  });
  $(".close").click(function(){
    $(this).parents(".overlay").fadeOut(400);
    if($(this).attr("data-reposition") != "no"){
      document.documentElement.scrollTop = scrollSource;//document.body.scrollTop
    }
  })
  /* Make the newTagFormShow */
  $(".addTag").click(function(){
    $("#newTagForm").fadeIn(400);
  });

  $(".oneMoreTag").click(function(){addExtraTagInput()});
  $(".oneMoreImg").click(function(){addExtraImage($(this))});
  $(".oneMoreVid").click(function(){addExtraVideo($(this))});

  $(".submitNewTagsForm").click(function(){
    submitNewTagForm();
    clearExtraTagInput();
    $("#newTagForm").fadeOut(400);
  });
  /* Filter projects */
  $(".projectsFilter").change(function(){filterProjects($(this))});
  /* Action listeners */
  var logInBtn = document.querySelector("#logInBtn");
  if(logInBtn){logInBtn.addEventListener("click", clickLogIn) };

  var loginCancel = document.querySelector("#logInCancelBtn");
  if(loginCancel){loginCancel.addEventListener("click", clickCancelLogIn)};

  var logOutBtn = document.querySelector("#logOutBtn");
  if(logOutBtn){logOutBtn.addEventListener("click", clickLogOut)};
  $(".bigVisible .visuals .gallery .control").click(function(e){e.stopPropagation()});
  $(".galleryPrev").click(function(){previousGallery($(this))});

  $(".galleryNext").click(function(){nextGallery($(this))});
  $(".addVisuals").click(function(){$("#"+$(this).attr("data-tgt")).fadeIn(200)})
  $(".deleteVis").click(function(){deleteVisual(this)});
  $(".imageInput").change(function(){getSignedRequest(this)});
  $(".submitNewVidsForm").click(function(){submitVisualsForm(this)});

  $(".newProject").click(function(){$("#newProject").fadeIn(400);})
  $(".submitProjectForm").click(function(){submitProjectForm($(this))});
  $(".deleteProjectForm").click(function(){deleteProject($(this))});

  $(".deleteTag").click(function(){deleteTag(this)});

  $(".showHeader").click(function(){
    $("header, .headerBackGround").fadeIn(200)
    $("header").css("display", "grid");
  });

  $(".headerBackGround").click(function(){
    $("header, .headerBackGround").fadeOut(200)
    $("header").css("display", "grid");
  });

  runBabyRun();
}
// Law


