/*var express = require('express');
var app = express();*/
const express = require('express')
const bodyParser = require('body-parser')
const store = require('./store')
const app = express()

const aws = require('aws-sdk');
let s3 = new aws.S3({
  accessKeyId: process.env.S3_KEY,
  secretAccessKey: process.env.S3_SECRET
});



app.use(express.static('public'))
// EXPERIMENTATION
const knex = require('knex')(require('./knexfile'))
// Use the session middleware
// https://www.npmjs.com/package/express-session
var session = require('express-session');
app.use(session({ secret: 'keyboard cat',
                  resave: true,
                  saveUninitialized: false,
                  cookie: { maxAge: 1000000 }}));
// Use bodyParser to get post requests and variables from the URL
//var bodyParser = require('body-parser'); 
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
// set the port of our application
// process.env.PORT lets the port be set by Heroku
app.set('port', (process.env.PORT || 5000))//5432)
// set the view engine to ejs
app.set('view engine', 'ejs');
// make express look in the public directory for assets (css/js/img)
app.use(express.static(__dirname + '/public'))

app.listen(5432, () => {
  //console.log('Server running on http://localhost:8000')
  console.log('Server running on http://localhost:5432')
})


/* Routes */

/* Create a user */
app.post('/createUser', (req, res) => {
  if (req.session && req.session.user_id && req.session.user_name) {
    console.log(req.body.email);
    console.log(req.body.password);
    store
      .createUser({
        email: req.body.email,
        password: req.body.password,
        name: req.body.name,
        access: req.body.access
      })
      .then(() => res.sendStatus(200))
  }else{
    res.status(401);
    res.send("You need to login baby...")
  }
})

/* Create a project */
app.post('/createProject', (req, res) => {
  if (req.session && req.session.user_id && req.session.user_name) {
    var project = req.body.project;
    var projtags = req.body.projtag;
    if(project.id == "new"){
      newProject(project, projtags, res);
    }else{
      updateProject(project, projtags, res);
    }
  }else{
    res.status(401);
    res.send("You need to login baby...")
  }
});

/* Delete a project */
app.post('/deleteProject', (req, res) => {
  if (req.session && req.session.user_id && req.session.user_name) {
    var project = req.body.project;
    deleteProject(project, res);
  }else{
    res.status(401);
    res.send("You need to login baby...")
  }
});

/* Create a tag */
app.post('/createTag', (req, res) => {
  if (req.session && req.session.user_id && req.session.user_name) {
    store
      .createTag({
        tagname: req.body.tagname
      })
      .then(() => res.sendStatus(200))
  }else{
    res.status(401);
    res.send("You need to login baby...")
  }
})

/* Delete a tag */
app.post('/deleteTag', (req, res) => {
  if (req.session && req.session.user_id && req.session.user_name) {
    var id = req.body.id;
    deleteTag(id, res);
  }else{
    res.status(401);
    res.send("You need to login baby...")
  }
});

app.post('/createTags', (req, res) => {
  if (req.session && req.session.user_id && req.session.user_name) {
    var arr = req.body.tagS;
    arr.forEach(function(tag) {
      store
        .createTag({
          tagname: tag.tagname
        })
        .then()
    });
    res.status(200);
    res.send({arr})
  }else{
    res.status(401);
    res.send("You need to login baby...")
  }
})

/* Create a project tag */
app.post('/createProjectTag', (req, res) => {
  if (req.session && req.session.user_id && req.session.user_name) {
    store
      .createProjectTag({
        projId: req.body.projId,
        tagId: req.body.tagId
      })
      .then(() => res.sendStatus(200))
  }else{
    res.status(401);
    res.send("You need to login baby...")
  }
})

/* Create an image */
app.post('/createVisual', (req, res) => {
  if (req.session && req.session.user_id && req.session.user_name) {
    store
      .createVisual({
        projId: req.body.projId,
        visDesc: req.body.visDesc,
        element: req.body.element,
        type: req.body.type
      })
      .then(() => res.sendStatus(200))
  }else{
    res.status(401);
    res.send("You need to login baby...")
  }
})

/* delete a visual */
app.post('/deleteVisual', (req, res) => {
  if (req.session && req.session.user_id && req.session.user_name) {
    var id = req.body.id;
    deleteVisual(id, res);
  }else{
    res.status(401);
    res.send("You need to login baby...")
  }
});

app.get('/test', function(request, response) {
  response.render('pages/home');
})

app.get('/', function(request, response) {
  var context = getContext(request);
  var vis = {};
  var tgs = {};
  var protgs = {};
  var prjs = []; // Has to be array to keep the cardorder
  // ,"user"
  knex.select("*").from("visual").then(function(visuals){
    visuals.map(function(v){
      if(!(v.projId in vis)){
        vis[v.projId] = {"img":[],"vid":[]};
      }
      vis[v.projId][v["type"]].push({'name':v.visDesc,'url':v.element, 'id':v.visId});
    });
    //console.log(vis);
    return knex.select("*").from("tag").then(function(tags){
      //console.log("Getting tags...")
      //console.log(tags)
      tags.map(function(tag){
        tgs[tag.id] = tag.tagname;
      });
      //console.log(tgs)
      context.tags = tgs;
      return knex.select("*").from("projtag").then(function(projtags){
        projtags.map(function(projtag){
          if(!(projtag.projId in protgs)){
            protgs[projtag.projId] = {};
          }
          protgs[projtag.projId][projtag.tagId] = tgs[projtag.tagId];
        });
        //console.log("Getting projtags...")
        //console.log(protgs)
        context.projtags = protgs;
        //response.render('pages/home', context);
        return knex.select("*").from("project").then(function(projects){
          projects.sort(function(a, b){
            return a.cardorder < b.cardorder ? -1 : 1;
          });
          projects.map(function(project){
            if(!(project.id in protgs)){
              protgs[project.id] = {};
            }
            project["tags"] = protgs[project.id];
            if(!(project.id in vis)){
              vis[project.id] = {"img":[],"vid":[]};
            }
            project["visuals"] = vis[project.id];
          });
          prjs = projects;
          /* CONTINUAR - Integrar os visuals com o HTML */
          context.projects = prjs;
          //console.log("Getting context...")
          /*console.log(prjs[3])
          console.log(vis)
          console.log(vis['3'].img)
          console.log(context.projects[3].visuals.img.length);*/
          response.render('pages/home', context);
          return true;
        })
      })
    });
  });
  //response.render('pages/home', context);
})

app.get('/2', function(request, response) {
   response.render('pages/home2', getContext(request));
})

/* Login routes */
app.post('/login', (req, res) => {
    var email = req.body.email;
    var password = req.body.password;
    store.authenticate({
      email: req.body.email,
      password: req.body.password
    })
    .then(({ success, uID, uName }) => {
      if (success) {
        req.session.user_id = uID;//T this will be the real user id
        req.session.user_name = uName;
        res.status(200);
        res.send({"modEmail":req.body.email,"modPass":req.body.password})
      }
        else {res.status(401);
        res.send('Wrong email/password.');
      }
    })
})

app.get('/logout', function(req, res, next) {
   if (req.session) {
      // delete session object
      req.session.destroy(function(err) {
         if(err) {
            return next(err);
         } else {
            return res.redirect('/');
         }
      });
   }
});

app.get('/tags', function(req, res, next) {
    var t;
    knex.select("*").from("tag","user")
      .then(function(tags, users){
        console.log("Getting tags...")
        console.log(tags)
        t = tags;
        return knex.select("*").from("user").then(function(users){
          console.log("Getting users...")
          console.log(users)
          res.status(200);
          res.send({"tags":t, "users":users})
          return true;
        })
      });

});

app.get('/logged', requiresLogin, function(request, response, next) {
  response.send('You are logged in!');
})
/* End of Login routes */

/* End of Routes */

app.listen(app.get('port'), function() {
  console.log("Node app is running at localhost:" + app.get('port'))
})

/* Middleware functions */

// If the user is logged in continue else throw error
function requiresLogin(request, response, next) {
   if (request.session && request.session.user_id) {
      return next();
   } else {
      var err = new Error('You must be logged in to view this page.');
      err.status = 401;
      return next(err);
   }
}

/* Support functions */
function getContext(request){
   if (request.session && request.session.user_id && request.session.user_name) {
      return {logStatus : true, user : {name : request.session.user_name, id : request.session.user_id}};
   } else {
      return {logStatus : false, user : {name : "", id : ""}};
   }
}

function newProject(project, projtags, res){
  delete project.id;
  knex('project').insert(project, "id").then(function(r){
    project.id = r[0];
    if(projtags){
      projtags = projtags.map(function(pt){
        pt.projId = project.id;
        return pt;
      });
    }
    return knex('projtag').insert(projtags)
  }).then(function(){
    res.status(200);
    res.send({"project":project, "projtags":projtags})
  }).catch(function(err){
    console.log(err);
    res.status(500).send(err);
  })
}

function updateProject(project, projtags, res){
  //deleteProjTagsFromProject(project.id);
  if(projtags){
    projtags = projtags.map(function(pt){
      pt.projId = project.id;
      return pt;
    });
  }
  
  knex('projtag').where('projId', project.id).del().then(function(del){
    knex('project').where({id: project.id}).update(project).then(function(r){
      return knex('projtag').insert(projtags)
    }).then(function(){
      res.status(200);
      res.send({"project":project, "projtags":projtags})
    }).catch(function(err){
      console.log(err);
      res.status(500).send(err);
    })
  });
    
}

// Here I have to delete the visual and projtags before the project
function deleteProject(project, res){
  knex('visual').where('projId', project.id).del().then(function(){
    knex('projtag').where('projId', project.id).del().then(function(){
       knex('project').where('id', project.id).del().then(function(){
        res.status(200);
        res.send("project deleted");
       });
    });
  }).catch(function(err){
    console.log(err);
    res.status(500).send(err);
  });
}

function deleteTag(id, res){
  knex('projtag').where('tagId', id).del().then(function(){
     knex('tag').where('id', id).del().then(function(){
      res.status(200);
      res.send("tag deleted");
     });
  }).catch(function(err){
    //console.log("ERRO p deletar TAG")
    console.log(err);
    res.status(500).send(err);
  });
}


function deleteProjTagsFromProject(projId){
  knex('projtag').where('projId', projId).del();
}

function deleteVisual(id, res){
  knex('visual').where('visId', id).del().then(function(){
    res.status(200);
    res.send("visual deleted");
  }).catch(function(err){
    console.log(err);
    res.status(500).send(err);
  });
}



/* -----------IMAGE UPLOAD-------------*/
aws.config.region = 'eu-west-2';

/*
 * Load the S3 information from the environment variables.
 */
const S3_BUCKET = process.env.S3_BUCKET;
/*
 * Respond to GET requests to /account.
 * Upon request, render the 'account.html' web page in views/ directory.
 */
//app.get('/account', (req, res) => res.render('account.html'));

/*
 * Respond to GET requests to /sign-s3.
 * Upon request, return JSON containing the temporarily-signed S3 request and
 * the anticipated URL of the image.
 */
app.get('/sign-s3', (req, res) => {
  const s3 = new aws.S3({
    accessKeyId: process.env.S3_KEY,
    secretAccessKey: process.env.S3_SECRET
  });
  const fileName = req.query['file-name'];
  const fileType = req.query['file-type'];
  const s3Params = {
    Bucket: S3_BUCKET,
    Key: fileName,
    Expires: 60,
    ContentType: fileType,
    ACL: 'public-read'
  };

  s3.getSignedUrl('putObject', s3Params, (err, data) => {
    if(err){
      console.log(err);
      return res.end();
    }
    const returnData = {
      signedRequest: data,
      url: `https://${S3_BUCKET}.s3.amazonaws.com/${fileName}`
    };
    /*res.write(returnData);//JSON.stringify(returnData));
    res.end();*/
    console.log(returnData.url);
    res.status(200);
    res.send(returnData);
  });
});

// Returns the signed request so the client can upload the img and save the detais on db
app.get('/get-s3-req', (req, res) => {
  if (req.session && req.session.user_id && req.session.user_name) {
    const s3 = new aws.S3({
      accessKeyId: process.env.S3_KEY,
      secretAccessKey: process.env.S3_SECRET
    });
    const fileName = req.query['file-name'];
    const fileType = req.query['file-type'];
    const projId = req.query['projId'];
    const s3Params = {
      Bucket: S3_BUCKET,
      Key: fileName,
      Expires: 60,
      ContentType: fileType,
      ACL: 'public-read'
    };

    s3.getSignedUrl('putObject', s3Params, (err, data) => {
      if(err){
        console.log(err);
        return res.end();
      }
      const returnData = {
        signedRequest: data,
        url: `https://${S3_BUCKET}.s3.amazonaws.com/${fileName}`
      };
      console.log(returnData.url);
      var visual = {};
      visual['element'] = returnData.url;
      visual['projId'] = projId;
      visual['visDesc'] = fileName;
      visual['type'] = "img";
      knex('visual').insert(visual).then(function(){
        res.status(200);
        res.send(returnData);
      }).catch(function(err){
        console.log(err);
        res.status(500).send(err);
      });
      
    });
  }else{
    res.status(401);
    res.send("You need to login baby...")
  }
});

app.post('/addVideo', (req, res) => {
  var visual = {};
  visual['element'] = req.body.element;
  visual['projId'] = parseInt(req.body.projId);
  visual['visDesc'] = req.body.desc;
  visual['type'] = "vid";
  knex('visual').insert(visual).then(function(){
    res.status(200);
    res.send("video saved");
  }).catch(function(err){
    console.log(err);
    res.status(500).send(err);
  });
});

/*
 * Respond to POST requests to /submit_form.
 * This function needs to be completed to handle the information in
 * a way that suits your application.
 */
app.post('/save-details', (req, res) => {
  // TODO: Read POSTed form data and do something useful
});

/* -----------END IMAGE UPLOAD-------------*/